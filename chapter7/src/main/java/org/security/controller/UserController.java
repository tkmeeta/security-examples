package org.security.controller;

import org.security.model.User;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by silentwu on 2015/5/21.
 */
@Controller
@RequestMapping("/user/")
public class UserController {


    @ResponseBody
    @RequestMapping(value = "register")
    public User register(@AuthenticationPrincipal Object user) {
        System.out.println(user);
        System.out.println(KeyGenerators.secureRandom().generateKey());
        System.out.println(KeyGenerators.string().generateKey());
        return new User().setName("silentwu").setAge(20);
    }


}
