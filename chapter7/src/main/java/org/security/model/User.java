package org.security.model;

/**
 * Created by silentwu on 2015/5/21.
 */
public class User {
    private String name;
    private int age;

    public User() {
    }

    public int getAge() {
        return age;
    }

    public User setAge(int age) {
        this.age = age;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }
}
