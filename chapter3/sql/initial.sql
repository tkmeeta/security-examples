CREATE TABLE users(
    username VARCHAR(50) NOT NULL PRIMARY KEY,
    PASSWORD VARCHAR(50) NOT NULL,
    enabled BOOLEAN NOT NULL
);

CREATE TABLE authorities (
    username VARCHAR(50) NOT NULL,
    authority VARCHAR(50) NOT NULL,
    CONSTRAINT fk_authorities_users FOREIGN KEY(username) REFERENCES users(username)
);
CREATE UNIQUE INDEX ix_auth_username ON authorities (username,authority);


insert  into `authorities`(`username`,`authority`) values ('silentwu','ROLE_USER');
insert  into `users`(`username`,`password`,`enabled`) values ('silentwu','12345',1);
